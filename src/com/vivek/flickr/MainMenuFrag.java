package com.vivek.flickr;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainMenuFrag extends ListFragment {
	LoginTryListener theloginhandleractivity;
	@Override
	public void onActivityCreated(Bundle icic) {
		super.onActivityCreated(icic);
		String[] thefrom = { "Flickr", "Picasa" };
		ArrayAdapter<String> theadapter = new ArrayAdapter<String>(
				getActivity(), R.layout.textlist,R.id.text1, thefrom);
		setListAdapter(theadapter);
	}
	 
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		try{
		theloginhandleractivity=(LoginTryListener)activity;
		}catch(ClassCastException e){
			 throw new ClassCastException(activity.toString() + " must implement LoginTry Listener");
		}
		
	}
	
	@Override
     public void onListItemClick(ListView l, View v, int position, long id) {
         theloginhandleractivity.handleLogin(position);
     }
	 
	 
	 
	 public interface LoginTryListener{
		 public void handleLogin(int i);
	 }
	 
	 
	

}
