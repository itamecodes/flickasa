package com.vivek.flickr;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class FlickrOpsMenu extends ListFragment{
	flickrActionsHandler theflickrActionsHandler;
	
	public static FlickrOpsMenu newInstance(){
		FlickrOpsMenu flOpsMenu=new FlickrOpsMenu();
		return flOpsMenu;
	}
	
	@Override
	public void onCreate(Bundle icic) {
		super.onCreate(icic);
		Log.v("flops","am here");
		String[] thefrom = { "View Photosets", "View Gallery","Pics Not in Photoset","Upload To FLickr" };
		ArrayAdapter<String> theadapter = new ArrayAdapter<String>(
				getActivity(), R.layout.textlist,R.id.text1, thefrom);
		
		setListAdapter(theadapter);
		
		ActionBar bar=getActivity().getActionBar();
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		Tab tab=bar.newTab().setText("Flickr").setTabListener(new FlickrTabListener(0));
		bar.addTab(tab,true);
		tab=bar.newTab().setText("Picasa").setTabListener(new FlickrTabListener(1));
		bar.addTab(tab,false);
		
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
	
		try{
			theflickrActionsHandler=(flickrActionsHandler)activity;
		}catch(ClassCastException e){
			 throw new ClassCastException(activity.toString() + " must implement LoginTry Listener");
		}
		
	}
	
	
	public interface flickrActionsHandler{
		public void pleaseHandletopTabs(int i);
		public void pleaseHandleFlickrLeftNav(int i);
	}
	
	public class FlickrTabListener implements ActionBar.TabListener{
		int i;
		public FlickrTabListener(int tabno) {
			i=tabno;
		}
		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			theflickrActionsHandler.pleaseHandletopTabs(i);
			
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			// TODO Auto-generated method stub
			
		}
		
	}

	@Override
	public void onListItemClick(ListView v, View vi, int pos, long id) {
		theflickrActionsHandler.pleaseHandleFlickrLeftNav(pos);
		
	}
	
}
