package com.vivek.flickr;

import java.util.HashMap;
import java.util.Map;

import org.scribe.model.Token;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class HandleOauthKeys {
	Activity c;
	Context context;
	final String ACCESS_TOKEN="accesstoken";
	final String ACCESS_TOKENSECRET="tokensecret";
	final String REQUEST_TOKEN="requesttoken";
	final String REQTOKEN_SECRET="reqsecret";
	final String USERID="userid";
	final String USERNAME="username";
	Map<String,String> myData=new HashMap<String,String>();
	Token theToken;
	
	SharedPreferences thestorage;
	
	public HandleOauthKeys(Activity act){
		this.c=act;
		this.context=act.getApplicationContext();
		thestorage=context.getSharedPreferences("FlicassaStorage", Context.MODE_PRIVATE);
	}
	
	public Token getAccessToken(){
		String thetoken=thestorage.getString(ACCESS_TOKEN,"notset");
		String thesecret=thestorage.getString(ACCESS_TOKENSECRET,"mysecret");
		Token accessToken=new Token(thetoken,thesecret);
		return accessToken;
		
	}
	public void setAccessToken(Token accessToken){
		String accesstoken=accessToken.getToken();
		String accesssecret=accessToken.getSecret();
		
		SharedPreferences.Editor theeditor=thestorage.edit();
		theeditor.putString(ACCESS_TOKEN, accesstoken);
		theeditor.putString(ACCESS_TOKENSECRET, accesssecret);
		theeditor.commit();
	}

	public void setRequestToken(Token reqToken){
		String reqtoken=reqToken.getToken();
		String reqsecret=reqToken.getSecret();
		SharedPreferences.Editor theeditor=thestorage.edit();
		theeditor.putString(REQUEST_TOKEN, reqtoken);
		theeditor.putString(REQTOKEN_SECRET, reqsecret);
		theeditor.commit();
	}
	public Token getRequestToken(){
		String thetoken=thestorage.getString(REQUEST_TOKEN,"notset");
		String thesecret=thestorage.getString(REQTOKEN_SECRET,"mysecret");
		Token reqToken=new Token(thetoken,thesecret);
		return reqToken;
		
	}
	public void setUser(String uid,String uname){
		SharedPreferences.Editor theeditor=thestorage.edit();
		theeditor.putString(USERID, uid);
		theeditor.putString(USERNAME, uname);
		theeditor.commit();
	}
	public Map<String,String> getUser(){
		String userid=thestorage.getString(USERID,"notset");
		String username=thestorage.getString(USERNAME,"mysecret");
		myData.put("userid",userid);
		myData.put("username",username);
		return myData;
	}
	public void clearData(){
		SharedPreferences.Editor theeditor=thestorage.edit();
		theeditor.clear();
		theeditor.commit();
	}
}
