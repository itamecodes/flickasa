package com.vivek.flickr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;

public class AdapterImageLoader {
	ExecutorService exservice;
	final int stub_id=R.drawable.stub;
	Context cont;
	private LruCache<String, Bitmap> myMemCache;
	private Map<ImageView,String> imageViews=Collections.synchronizedMap(new WeakHashMap<ImageView,String>());
	int diskCacheSize;
    Filecache filecache;
    int height;
    int width;
	public AdapterImageLoader(Context c){
		
		cont=c;
		exservice=Executors.newFixedThreadPool(5);
		final int memClass=((ActivityManager)cont.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
		final int cacheSize=(1024*1024*memClass)/8;
		diskCacheSize=1024*1024*10;//10MB
		
		myMemCache=new LruCache<String, Bitmap>(cacheSize){
			@Override
			protected int sizeOf(String key, Bitmap value) {
		        return (value.getRowBytes() * value.getHeight());
		    }
		};
		filecache=new Filecache(c);
		//filecache.clear();
	}
	/**utils*/
	public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
	    if (getBitmapFromMemCache(key) == null) {
	    	myMemCache.put(key, bitmap);
	    }
	}

	public Bitmap getBitmapFromMemCache(String key) {
	    return (Bitmap) myMemCache.get(key);
	}
	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
	        int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);
	    Log.v("stubb",options.outWidth+" "+options.outHeight);
	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
	    Log.v("stubbnew",options.inSampleSize+"");
	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    Bitmap image=BitmapFactory.decodeResource(res, resId, options);
	    Bitmap finalImage=Bitmap.createScaledBitmap(image,46,69,false); 
	    Log.v("stubbnew",finalImage.getWidth()+" "+finalImage.getHeight());
	    return finalImage;
	}
	public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {
        if (width > height) {
        	Log.v("width",height+"/"+reqHeight);
            inSampleSize = Math.round((float)height / (float)reqHeight);
        } else {
            inSampleSize = Math.round((float)width / (float)reqWidth);
        }
    }
    return inSampleSize;
}

public static Bitmap decodeSampledBitmapFromResource(BufferedHttpEntity bfe,int reqWidth, int reqHeight) {
	InputStream is=null;
	try {
		is = bfe.getContent();
	} catch (IOException e2) {
		e2.printStackTrace();
	}
    // First decode with inJustDecodeBounds=true to check dimensions
    final BitmapFactory.Options options = new BitmapFactory.Options();
    options.inJustDecodeBounds = true;
    BitmapFactory.decodeStream(is,null,options);
    Log.v("stubbforimagesbefore",options.outWidth+" "+options.outHeight);
    // Calculate inSampleSize
    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
    Log.v("stubbimagessample",options.inSampleSize+"");
    try {
		is.close();
	} catch (IOException e1) {
		e1.printStackTrace();
	}
    InputStream res=null;
	try {
		res = bfe.getContent();
	} catch (IOException e) {
		e.printStackTrace();
	}
    // Decode bitmap with inSampleSize set
    options.inJustDecodeBounds = false;
    Bitmap mylocimage=BitmapFactory.decodeStream(res,null,options);
    Log.v("stubbforimages",mylocimage.getWidth()+" "+mylocimage.getHeight());
    Bitmap finalImage=Bitmap.createScaledBitmap(mylocimage,reqWidth,200,false); 
    return finalImage;
}

/*utils ends*/


	public void DisplayImage(String url,ImageView view,int reqWidth,int reqHeight){
		imageViews.put(view, url);
		height=reqHeight;
		width=reqWidth;
		Bitmap image=getBitmapFromMemCache(url);
		if(image!=null){
			Log.v("mymem","got from mem");
			view.setImageBitmap(image);
			return;
		}else{
			Log.v("queuing","am qued"+url);
			queueImage(url,view);
			view.setImageBitmap(decodeSampledBitmapFromResource(cont.getResources(), stub_id, width, height));
		}
	}
	private void queueImage(String url,ImageView view){
		ImageToLoad imgtoLoad=new ImageToLoad(url,view);
		exservice.submit(new ImageLoader(imgtoLoad));
		
	}
	private class ImageToLoad{
		public String url;
		public ImageView imageView;
		public ImageToLoad(String theurl,ImageView view){
			url=theurl;
			imageView=view;
		}
	}
	
	boolean viewReused(ImageToLoad theimgobj){
		String tag=imageViews.get(theimgobj.imageView);
		if(tag==null || !tag.equals(theimgobj.url)){
			return true;
		}
		return false;
	}

	private Bitmap getBitmap(String url){
		Bitmap image=null;
		File f=filecache.getFile(url);
		try {
			image=BitmapFactory.decodeStream(new FileInputStream(f));
			Log.v("filec","got fromfilecah");
		} catch (FileNotFoundException e) {
			//e.printStackTrace();
		}
		
		
		if(image!=null){
			return image;
		}
		try {
			Log.v("imagenotfoun",""+url);
			HttpClient hc=MyHttpUtils.getThreadSafeClient();
			HttpGet hg=new HttpGet(url);
			HttpResponse hr=hc.execute(hg);
			HttpEntity he=hr.getEntity();
			BufferedHttpEntity bfe=new BufferedHttpEntity(he);
			image=decodeSampledBitmapFromResource(bfe,width,height);
			Log.v("reused","adding to memcahce"+url);
			addBitmapToMemoryCache(url,image);
			OutputStream outStream=new FileOutputStream(f);
			image.compress(Bitmap.CompressFormat.PNG, 100, outStream);
		    outStream.flush();
		    outStream.close();
			return image;
		} catch (ClientProtocolException e) {
			
		} catch (IOException e) {
			
		}
		return null;
	}
	
	
	class ImageLoader implements Runnable{
		ImageToLoad imgtoLoad;
		ImageLoader(ImageToLoad imgobj){
			imgtoLoad=imgobj;
		}
		@Override
		public void run() {
			
			if(viewReused(imgtoLoad)){
				Log.v("reused","returning");
				return;
			}
			final Bitmap image=getBitmap(imgtoLoad.url);
			if(viewReused(imgtoLoad)){
				Log.v("reused","returning");
				return;
			}
			Activity callingAct=(Activity)imgtoLoad.imageView.getContext();
			callingAct.runOnUiThread(new Runnable(){

				@Override
				public void run() {
					if(viewReused(imgtoLoad)){
						return;
					}
					if(image!=null){
						imgtoLoad.imageView.setImageBitmap(image);
					}else{
						imgtoLoad.imageView.setImageResource(stub_id);
					}
					
				}
				
			});
		}
		
	}
	
}
