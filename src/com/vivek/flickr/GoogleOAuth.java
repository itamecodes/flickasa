package com.vivek.flickr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

public class GoogleOAuth implements Runnable {
	
	private final String APIKEY="1032994939345.apps.googleusercontent.com";
	private final String APISECRET="SWDPF9dx8UtZpCe4bdXRq6lT";
	private final String REDIRECTURI="urn:ietf:wg:oauth:2.0:oob";
	private final String SCOPE="https://www.googleapis.com/auth/userinfo.profile https://picasaweb.google.com/data/";
	private final String USERCODEURL="https://accounts.google.com/o/oauth2/device/code";
	private final String ACCESSCODEURL="https://accounts.google.com/o/oauth2/token";
	private final String GRANTTYPE="http://oauth.net/grant_type/device/1.0";
	
	String DeviceCode;
	
	Activity c;
	String at,rt,it;
	
	Handler handler;
	TextView tv;
	boolean gotaccess=false;
	HandleOauthKeysGoogle thekeyHandler;
	String myRefreshToken;
	
	public GoogleOAuth(Activity act,Handler myhandler,TextView theview){
		this(act,myhandler,theview,null);
		
	}
	public GoogleOAuth(Activity act,Handler myhandler,TextView theview,String reftoken){
		c=act;
		handler=myhandler;
		tv=theview;
		thekeyHandler=new HandleOauthKeysGoogle(c);
		myRefreshToken=reftoken;
		
	}
	
	@Override
	public void run(){
		if(myRefreshToken==null){
			getUserCode();
		}else{
			 final Timer timer=new Timer();
		        TimerTask task=new TimerTask() {
					
					@Override
					public void run() {
						if(!(gotaccess)){
							getRefreshedAccessToken();
							
						}else{
							timer.cancel();
							Message themessage=handler.obtainMessage(2);
					        themessage.obj=at;
					        handler.sendMessage(themessage);
						}
					}
				};
				timer.schedule(task, 1000,6000);
			
		}
		
	}
	
	public void getUserCode(){
		HttpClient hc=MyHttpUtils.getThreadSafeClient();
		HttpPost hp=new HttpPost(USERCODEURL);
	    try {
	        // Add your data
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("client_id",APIKEY));
	        nameValuePairs.add(new BasicNameValuePair("scope",SCOPE));
	        UrlEncodedFormEntity ent=new UrlEncodedFormEntity(nameValuePairs);
	        /*
	         //uncomment the below when you want to see what the post data is being sent the encoding etc 
	        InputStream ist=ent.getContent();
	        BufferedReader brt=new BufferedReader(new InputStreamReader(ist));
	        String lines=null;
	        StringBuilder sbr=new StringBuilder();
	        while((lines=brt.readLine())!=null){
	        	sbr.append(lines);
	        }
	        Log.v("viv",sbr.toString());
	        */
	        hp.setEntity(ent);
	        
	        // Execute HTTP Post Request
	        HttpResponse response = hc.execute(hp);
	        HttpEntity he=response.getEntity();
	        InputStream is=he.getContent();
	        BufferedReader br=new BufferedReader(new InputStreamReader(is));
	        String line=null;
	        StringBuilder sb=new StringBuilder();
	        while((line=br.readLine())!=null){
	        	sb.append(line);
	        }
	        String result=sb.toString();
	        //now create a json object
	        JSONObject jo=new JSONObject(result);
	        final String usercode=jo.getString("user_code");
	        String veriurl=jo.getString("verification_url");
	        DeviceCode=jo.getString("device_code");
	        c.runOnUiThread(new Runnable(){

				public void run() {
					tv.setText("Please enter '"+usercode+"' in the below window");
					
				}
	        	
	        });
	        Message themessage=handler.obtainMessage(1);
	        themessage.obj=veriurl;
	        handler.sendMessage(themessage);
	        final Timer timer=new Timer();
	        TimerTask task=new TimerTask() {
				
				@Override
				public void run() {
					if(!(gotaccess)){
						getAccess();
						
					}else{
						timer.cancel();
						Message themessage=handler.obtainMessage(2);
				        themessage.obj=new MessageObjGoogle(at,rt,it);
				        handler.sendMessage(themessage);
					}
				}
			};
			timer.schedule(task, 60000,6000);
	        
	    } catch (ClientProtocolException e) {
	        Log.v("errr1",e.getLocalizedMessage());
	    } catch (IOException e) {
	    	Log.v("errr2",e.getLocalizedMessage());
	    }catch(JSONException e){
	    	Log.v("errr3",e.getLocalizedMessage());
	    }
	}
	public void getAccess(){
		HttpClient hc=MyHttpUtils.getThreadSafeClient();
		HttpPost hp=new HttpPost(ACCESSCODEURL);
		try{
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
	        nameValuePairs.add(new BasicNameValuePair("client_id",APIKEY));
	        nameValuePairs.add(new BasicNameValuePair("client_secret",APISECRET));
	        nameValuePairs.add(new BasicNameValuePair("code",DeviceCode));
	        nameValuePairs.add(new BasicNameValuePair("grant_type",GRANTTYPE));
	        UrlEncodedFormEntity ent=new UrlEncodedFormEntity(nameValuePairs);
	        InputStream ist=ent.getContent();
	        BufferedReader brt=new BufferedReader(new InputStreamReader(ist));
	        
	        String lines=null;
	        StringBuilder sbr=new StringBuilder();
	        while((lines=brt.readLine())!=null){
	        	sbr.append(lines);
	        }
	        Log.v("viv",sbr.toString());
	        
	        hp.setEntity(ent);
	        HttpResponse response = hc.execute(hp);
	        HttpEntity he=response.getEntity();
	        InputStream is=he.getContent();
	        BufferedReader br=new BufferedReader(new InputStreamReader(is));
	        String line=null;
	        StringBuilder sb=new StringBuilder();
	        while((line=br.readLine())!=null){
	        	sb.append(line);
	        }
	        String result=sb.toString();
	        Log.v("output",result);
	        //now create a json object
	        JSONObject jo=new JSONObject(result);
	        try{
	        	at=jo.getString("access_token");
	        	rt=jo.getString("refresh_token");
	        	it=jo.getString("id_token");
	        	gotaccess=true;
	        	Log.v("atrtit",at+"--->"+rt+"--->"+it);
	        	thekeyHandler.setAccessToken(at, rt, it);
	        }catch(JSONException e){
	        	String error=jo.getString("error");
	        	Log.v("errr",error);
	        }
		}catch (ClientProtocolException e) {
	       Log.v("errr1",e.getLocalizedMessage());
	    } catch (IOException e) {
	    	Log.v("errr2",e.getLocalizedMessage());
	    }catch(JSONException e){
	    	Log.v("errr3",e.getLocalizedMessage());
	    }
	}
	
	public void getRefreshedAccessToken(){
		Log.v("ref","getting new acct");
		HttpClient hc=new DefaultHttpClient();
		HttpPost hp=new HttpPost(ACCESSCODEURL);
		try{
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
	        nameValuePairs.add(new BasicNameValuePair("client_id",APIKEY));
	        nameValuePairs.add(new BasicNameValuePair("client_secret",APISECRET));
	        nameValuePairs.add(new BasicNameValuePair("refresh_token",myRefreshToken));
	        nameValuePairs.add(new BasicNameValuePair("grant_type","refresh_token"));
	        UrlEncodedFormEntity ent=new UrlEncodedFormEntity(nameValuePairs);
	        InputStream ist=ent.getContent();
	        BufferedReader brt=new BufferedReader(new InputStreamReader(ist));
	        
	        String lines=null;
	        StringBuilder sbr=new StringBuilder();
	        while((lines=brt.readLine())!=null){
	        	sbr.append(lines);
	        }
	        Log.v("viv",sbr.toString());
	        
	        hp.setEntity(ent);
	        HttpResponse response = hc.execute(hp);
	        /*
	         The below code is for getting statue code of a response
	        StatusLine statusline=response.getStatusLine();
	        int statCode=statusline.getStatusCode();
	        */
	        HttpEntity he=response.getEntity();
	        
	        InputStream is=he.getContent();
	        BufferedReader br=new BufferedReader(new InputStreamReader(is));
	        String line=null;
	        StringBuilder sb=new StringBuilder();
	        while((line=br.readLine())!=null){
	        	sb.append(line);
	        }
	        String result=sb.toString();
	        Log.v("output",result);
	        JSONObject jo=new JSONObject(result);
	        try{
	        	at=jo.getString("access_token");
	        	String it=jo.getString("id_token");
	        	gotaccess=true;
	        	thekeyHandler.setAccessToken(at, myRefreshToken, it);
	        }catch(JSONException e){
	        	String error=jo.getString("error");
	        	Log.v("errr",error);
	        }
			
		}catch (ClientProtocolException e) {
		       Log.v("errr1",e.getLocalizedMessage());
		    } catch (IOException e) {
		    	Log.v("errr2",e.getLocalizedMessage());
		    }catch(JSONException e){
		    	Log.v("errr3",e.getLocalizedMessage());
		    }
	}
	
	
}
