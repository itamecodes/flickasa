package com.vivek.flickr;



import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import com.vivek.flickr.FlickrFragment.FlickrShowPhotos;
import com.vivek.flickr.FlickrOpsMenu.flickrActionsHandler;
import com.vivek.flickr.MainMenuFrag.LoginTryListener;

public class FlicassaHome extends Activity implements LoginTryListener,flickrActionsHandler,FlickrShowPhotos{
	TextView picasa,flickr;
	HandleOauthKeys handleKeys;
	HandleOauthKeysGoogle handleGoogleKeys;
	ExecutorService es=Executors.newFixedThreadPool(1);
	Token requestToken,accessToken;
	OAuthService service;
	Intent newIntent;
	Uri calluri;
	String userId;
	private static final int FLICKR=1;
	private static final int PICASA=2;
	private static final String PROTECTED_RESOURCE_URL = "http://api.flickr.com/services/rest/";
	
	Handler myhandler=new Handler(){
		@Override
		public void handleMessage(Message message){
			switch(message.what){
			case 4:{
				String authorizationUrl=(String)message.obj;
				Intent browserIntent = new Intent(Intent.ACTION_VIEW,Uri.parse(authorizationUrl));
				startActivity(browserIntent);
				break;
			}
			case 1:{
				es.shutdown();
				loadRightPane("getPhotoSetsOfUser");
				//Intent r=new Intent(FlicassaHome.this,FlickrActivity.class);
				//FlicassaHome.this.startActivity(r);
			}
			case 3:{
				int mystatCode=message.arg1;
				Log.v("thestatusfromflicassa",mystatCode+"");
				break;
			}
			}
		}
	};
	@Override
	public void onCreate(Bundle icic){
		super.onCreate(icic);
		setContentView(R.layout.main);
		
		MainMenuFrag leftMenu=new MainMenuFrag();
		FragmentTransaction ft=getFragmentManager().beginTransaction();
		ft.replace(R.id.themainmenuleft, leftMenu,"mainleftmenu");
		ft.commit();
	}

	public void handleLogin(int theviewid) {
		switch(theviewid){
		case 0:{
			handleKeys=new HandleOauthKeys(this);
			//handleKeys.clearData();
			
			Token myaccessToken = handleKeys.getAccessToken();
			String theaccessToken = myaccessToken.getToken();
			if (theaccessToken.equals("notset")){
				service = new ServiceBuilder().provider(FlickrNewApi.class).callback("vivek://com.life.flicasa").apiKey(Config.FLAPIKEY.getValue())
						.apiSecret(Config.FLAPISECRET.getValue()).debug().build();
				Log.v("viv","goin in");
				doFirstLogin();
			}else{
				Map<String, String> theuserDetails = handleKeys.getUser();
				userId = theuserDetails.get("userid");
				loadLeftPane();
				loadRightPane("getPhotoSetsOfUser","5","1",userId);
				//Intent r=new Intent(this.getApplicationContext(),FlickrActivity.class);
				//this.startActivity(r);
			}
			break;
		}
		case 1:{
			//check for access token
			handleGoogleKeys=new HandleOauthKeysGoogle(this);
			HashMap<String,String> googHm=handleGoogleKeys.getAccessToken();
			String googAt=googHm.get("aToken");
			if(googAt=="notset"){
				//if no accesstoken start PicasaFragment and aask for accesstoken
				
			}else{
				final String testUrl="https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token="+googAt;
				Thread testLifeAt=new Thread(){
					@Override
					public void run(){
						HttpClient hc=MyHttpUtils.getThreadSafeClient();
						HttpGet hg=new HttpGet(testUrl);
						HttpResponse hr=null;
						try {
							hr=hc.execute(hg);
						} catch (ClientProtocolException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						StatusLine statusline=hr.getStatusLine();
				        int statCode=statusline.getStatusCode();
				        Message themes=myhandler.obtainMessage(3, statCode,0);
				        myhandler.sendMessage(themes);
					}
				};
			}
				
				//if accesstoken present
					//check if accesstoken has expired by sending some request .if response is 401 ->means accessToken has expired
			//if everything is ok then proceed to show albums
			Intent r=new Intent(this.getApplicationContext(),PicasaActivity.class);
			this.startActivity(r);
			break;
		}
		}
		
	}
	
	public void loadRightPane(String callback,String... stringargs){
		Log.v("callback is",callback);
		Log.v("stringargs are ",stringargs.toString());
		FlickrFragment rightFragment=(FlickrFragment)getFragmentManager().findFragmentById(R.id.therightmenu);
		if(rightFragment==null || rightFragment.getCallback()!=callback){
			rightFragment=FlickrFragment.newInstance(callback,stringargs);
			FragmentTransaction ft=getFragmentManager().beginTransaction();
			ft.replace(R.id.therightmenu,rightFragment);
			ft.commit();
		}
	}
	
	
	public void loadLeftPane(){
		Log.v("flops","am in loadleftpane");
		FlickrOpsMenu flickropsmenu=(FlickrOpsMenu)getFragmentManager().findFragmentByTag("flickropsmenu");
		if(flickropsmenu==null){
			Log.v("flops","flopsmenu is null");
			flickropsmenu=FlickrOpsMenu.newInstance();
			FragmentTransaction ft=getFragmentManager().beginTransaction();
			ft.replace(R.id.themainmenuleft, flickropsmenu,"flickropsmenu");
			ft.commit();
		}
	}
	
	@Override
	public void onNewIntent(final Intent newintent) {
		newIntent = newintent;
		calluri = newIntent.getData();
		if (calluri != null && calluri.toString().startsWith("vivek")) {
			
			Thread accessTokenThread=new Thread(){
				@Override
				public void run(){
					getAndStoreAccessToken();
				}
			};
			
		es.execute(accessTokenThread);
			

		}
	}
	
	public void doFirstLogin() {
		Log.v("viv","goin in2");
		Thread theloginThread=new Thread(){
			@Override
			public void run(){
				Log.v("viv","goin in3");
				getFirstRequestToken();
			}
		};
		es.execute(theloginThread);
	}
	
	public void getFirstRequestToken(){
		requestToken = service.getRequestToken();
		handleKeys.setRequestToken(requestToken);
		String authorizationUrl = service.getAuthorizationUrl(requestToken);
		Message themessage=myhandler.obtainMessage(4,authorizationUrl);
		myhandler.sendMessage(themessage);
	}
	public void getAndStoreAccessToken(){
		String verifier = calluri.getQueryParameter("oauth_verifier");
		Verifier v = new Verifier(verifier);
		requestToken = handleKeys.getRequestToken();
		// now use this verifier and the request token got earlier to obtain
		// the access token and
		// please save the access token
		accessToken = service.getAccessToken(requestToken, v);
		handleKeys.setAccessToken(accessToken);

		OAuthRequest request = new OAuthRequest(Verb.GET,PROTECTED_RESOURCE_URL);
		request.addQuerystringParameter("method", "flickr.test.login");
		request.addQuerystringParameter("format", "json");

		service.signRequest(accessToken, request);
		Log.v("viv", (request.getQueryStringParams()).toString());

		Response response = request.send();
		String formatResponse = response.getBody();
		formatResponse = formatResponse.replace("jsonFlickrApi(", "");
		formatResponse = formatResponse.substring(0,formatResponse.length() - 1);
		String userDetails = formatResponse;
		Log.v("res", userDetails);
		try {
			JSONObject jo = new JSONObject(userDetails);
			JSONObject userJO = jo.getJSONObject("user");
			String userId = userJO.getString("id");
			JSONObject usernameJO = userJO.getJSONObject("username");
			String userName = usernameJO.getString("_content");
			handleKeys.setUser(userId, userName);
			
		} catch (JSONException e) {

		}
		myhandler.sendEmptyMessage(1);
	}
	
	public void upload(){
		
	}

	@Override
	public void pleaseHandletopTabs(int i) {
		switch(i){
		case 0:{
			Log.v("handler","flickr clicked");
			break;
		}
		case 1:{
			Log.v("handler","picasa clicked");
			break;
		}
		}
		
	}
	


	@Override
	public void showPhotos(String i) {
		loadRightPane("showImagesFromPhotoset",i,"5","1",userId);
		
	}
	@Override
	public void showPhotosFromGallery(String i) {
		loadRightPane("getPhotosFromGallery",i,"5","1",userId);
		
	}

	@Override
	public void pleaseHandleFlickrLeftNav(int i) {
		switch(i){
		case 0:{
			loadRightPane("getPhotoSetsOfUser","5","1",userId);
			break;
		}
		case 1:{
			loadRightPane("getGalleries",userId,"1","5");
			break;
		}
		case 2:{
			loadRightPane("getPhotoNotInSetOfUser");
			break;
		}
		case 3:{
			break;
		}
		}
		
	}



}
