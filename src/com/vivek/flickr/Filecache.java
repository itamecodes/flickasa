package com.vivek.flickr;

import java.io.File;
import java.net.URLEncoder;

import android.content.Context;
import android.util.Log;

public class Filecache {
	private File cacheDir;
	public Filecache(Context c){
		if(android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"MyImageCacheNewNew");
		}else{
			cacheDir=c.getCacheDir();
		}
		if(!cacheDir.exists()){
			boolean rt=cacheDir.mkdirs();
			Log.v("cachedir","no cache"+cacheDir.getAbsolutePath()+"-"+rt);
		}
	}
	
	public File getFile(String url){
		String filename=URLEncoder.encode(url);
		File f=new File(cacheDir,filename);
		return f;
	}
	
	public void clear(){
		File[] files=cacheDir.listFiles();
		if(files==null){
			Log.v("fcgreat","am null");
			return;
		}
		for(File fi:files){
			Log.v("fcgreat","am deleting");
			fi.delete();
		}
	}
	
	
	
}
