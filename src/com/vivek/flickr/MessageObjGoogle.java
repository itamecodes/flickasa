package com.vivek.flickr;

public class MessageObjGoogle {
	private String at;
	private String rt;
	private String it;
	public MessageObjGoogle(String at,String rt,String it){
		this.at=at;
		this.rt=rt;
		this.it=it;
	}
	
	public String getAt(){
		return at;
	}
	public String getRt(){
		return rt;
	}
	public String getIt(){
		return it;
	}
	
}
