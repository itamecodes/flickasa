package com.vivek.flickr;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;

public class FlickrRest implements Runnable {
	String apiKey = "496a8ac6ca46325e6cefcb924d539296";
	String apiSecret = "685682a92c03ebdc";
	private static final String PROTECTED_RESOURCE_URL = "http://api.flickr.com/services/rest/";
	Activity c;
	OAuthService service;
	Token accessToken;
	OAuthRequest request;
	String themethod;
	String[] args;
	JSONObject jo;
	Handler myhandler;
	ArrayList<String> imageArray;

	public FlickrRest(Activity act, OAuthService theservice, Token aT,
			String callback,Handler thehandler,ArrayList<String> urls) {
		c = act;
		accessToken = aT;
		service = theservice;
		themethod = callback;
		myhandler=thehandler;
		request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
		imageArray=urls;
	}

	@Override
	public void run() {
		try {
			Method method = this.getClass().getMethod(themethod);
			method.invoke(this);
		} catch (SecurityException e) {
			Log.v("viver", "sc" + e.getLocalizedMessage());
		} catch (NoSuchMethodException e) {
			Log.v("viver1", "sc" + e.getLocalizedMessage());
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void getActivityUserPhotos() {
		String method = "flickr.activity.userPhotos";
		request.addQuerystringParameter("method", method);
		request.addQuerystringParameter("format", "json");

		service.signRequest(accessToken, request);
		Response response = request.send();
		Log.v("vivek", response.getBody());
	}

	public void getActivityPhotosCommentedOn() {
		String method = "flickr.activity.userComments";
		request.addQuerystringParameter("method", method);
		request.addQuerystringParameter("format", "json");

		service.signRequest(accessToken, request);
		Response response = request.send();
		Log.v("vivek", response.getBody());
	}

	public void getGalleriesListOfUser(String userid, String per_page,
			String pageno) {
		String method = "flickr.galleries.getList";
		request.addQuerystringParameter("method", method);
		request.addQuerystringParameter("format", "json");
		request.addQuerystringParameter("user_id", userid);
		request.addQuerystringParameter("api_key", apiKey);
		request.addQuerystringParameter("per_page", per_page);
		request.addQuerystringParameter("page", pageno);
		Response response = request.send();
		Log.v("vivek", response.getBody());
	}

	public void getPhotoNotInSetOfUser() {
		String method = "flickr.photos.getNotInSet";
		request.addQuerystringParameter("method", method);
		request.addQuerystringParameter("format", "json");
		request.addQuerystringParameter("api_key", apiKey);
		request.addQuerystringParameter("per_page", "20");
		service.signRequest(accessToken, request);
		Response response = request.send();
		String locResp = response.getBody().replace("jsonFlickrApi(", "");
		locResp = locResp.substring(0, locResp.length() - 1);
		Log.v("locresp",locResp);
		try {
			jo = new JSONObject(locResp);
			JSONObject jPhotos=jo.getJSONObject("photos");
			Log.v("vivek","1");
			String pageno=jPhotos.getString("page");
			Log.v("vivek","2"+pageno);
			String totpages=jPhotos.getString("pages");
			Log.v("vivek","3"+totpages);
			String totPhots=jPhotos.getString("total");
			Log.v("vivek","4"+totPhots);
			JSONArray ja=jPhotos.getJSONArray("photo");
			Log.v("vivek","5");
			String url="http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg";
			for(int i=0;i<(ja.length()-1);i++){
				JSONObject locJo=ja.getJSONObject(i);
				String imgid=locJo.getString("id");
				String secret=locJo.getString("secret");
				String server=locJo.getString("server");
				String farm=locJo.getString("farm");
				String theurl="http://farm"+farm+".staticflickr.com/"+server+"/"+imgid+"_"+secret+".jpg";
				imageArray.add(theurl);
				
			}
			/*
			 * 
			 * jsonFlickrApi( {"photos":{"page":1, "pages":4, "perpage":20,
			 * "total":"77", "photo":[ {"id":"6239439357",
			 * "owner":"68584477@N05", "secret":"0b722b106c", "server":"6233",
			 * "farm":7, "title":"DSC01368", "ispublic":0, "isfriend":0,
			 * "isfamily":1}, {"id":"6239440863", "owner":"68584477@N05",
			 * "secret":"c2e6c7c713", "server":"6169", "farm":7,
			 * "title":"DSC01369", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239442175", "owner":"68584477@N05",
			 * "secret":"b761fe1fe3", "server":"6155", "farm":7,
			 * "title":"DSC01370", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239960126", "owner":"68584477@N05",
			 * "secret":"214c95edf7", "server":"6163", "farm":7,
			 * "title":"DSC01371", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239961272", "owner":"68584477@N05",
			 * "secret":"bf410a3c3f", "server":"6055", "farm":7,
			 * "title":"DSC01372", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239445855", "owner":"68584477@N05",
			 * "secret":"899ef92253", "server":"6222", "farm":7,
			 * "title":"DSC01373", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239447463", "owner":"68584477@N05",
			 * "secret":"8a71b5a787", "server":"6217", "farm":7,
			 * "title":"DSC01378", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239965386", "owner":"68584477@N05",
			 * "secret":"7444763105", "server":"6212", "farm":7,
			 * "title":"DSC01379", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239966400", "owner":"68584477@N05",
			 * "secret":"53148d860b", "server":"6162", "farm":7,
			 * "title":"DSC01381", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239967414", "owner":"68584477@N05",
			 * "secret":"173e2fbf5b", "server":"6119", "farm":7,
			 * "title":"DSC01382", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239451751", "owner":"68584477@N05",
			 * "secret":"44d328323f", "server":"6226", "farm":7,
			 * "title":"DSC01380", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239452835", "owner":"68584477@N05",
			 * "secret":"a4aa2203b8", "server":"6234", "farm":7,
			 * "title":"DSC01383", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239453751", "owner":"68584477@N05",
			 * "secret":"9258751d08", "server":"6109", "farm":7,
			 * "title":"DSC01384", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239971380", "owner":"68584477@N05",
			 * "secret":"b5a2c8de18", "server":"6221", "farm":7,
			 * "title":"DSC01386", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239972148", "owner":"68584477@N05",
			 * "secret":"feabf523ba", "server":"6093", "farm":7,
			 * "title":"DSC01387", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239456577", "owner":"68584477@N05",
			 * "secret":"1c4b659b10", "server":"6239", "farm":7,
			 * "title":"DSC01388", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239974194", "owner":"68584477@N05",
			 * "secret":"34c0161621", "server":"6233", "farm":7,
			 * "title":"DSC01389", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239975290", "owner":"68584477@N05",
			 * "secret":"72a76ef931", "server":"6039", "farm":7,
			 * "title":"DSC01390", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239459769", "owner":"68584477@N05",
			 * "secret":"56d82a0df6", "server":"6160", "farm":7,
			 * "title":"DSC01392", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239460755", "owner":"68584477@N05",
			 * "secret":"077cdc4cf9", "server":"6096", "farm":7,
			 * "title":"DSC01391", "ispublic":0, "isfriend":0, "isfamily":1}]},
			 * "stat":"ok"})
			 */
			
		} catch (JSONException e) {
			Log.v("vivek", "unable to create the main json object"+e.getLocalizedMessage());
		}
		myhandler.sendEmptyMessage(2);
		Log.v("vivek", response.getBody());
	}

}
