package com.vivek.flickr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

public class FlickrFragment extends Fragment implements OnItemClickListener{
	TextView tv;
	WebView wv;
	GridView thegrid;

	JSONObject jo;
	Intent newIntent;
	Config theconfig;

	Uri calluri;

	Token requestToken;
	Token accessToken;
	OAuthService service;

	
	FlickrShowPhotos theflickrShowPhotos;
	HandleOauthKeys handleKeys;
	LazyImageAdapter theimageAdapter;

	ExecutorService es = Executors.newFixedThreadPool(1);
    ArrayList<HashMap<String,String>> urls = new ArrayList<HashMap<String,String>>();
	private static Dialog busyDialog;
	static int i = 0;

	Handler myhandler=new Handler() {
		@Override
		public void handleMessage(Message message) {
			switch (message.what) {
			case 2: {
				Log.v("vivek", "changing data");
				ArrayList<HashMap<String,String>> theurls =((MessageObj)message.obj).getArrayList();
				//Log.v("alisy", message.obj.toString());
				urls.addAll(theurls);
				theimageAdapter.notifyDataSetChanged();
				dismissBusyDialog();
				break;
			}
		

			}

		}
	};;
	
	public static FlickrFragment newInstance(String callback,String... stringargs){
		FlickrFragment flFr=new FlickrFragment();
		Bundle args=new Bundle();
		args.putString("callback",callback);
		args.putStringArray("extraargs",stringargs);
		flFr.setArguments(args);
		return flFr;
	}
	
	public String getCallback(){
		return getArguments().getString("callback");
	}
    
	public String[] getExtraargs(){
		return getArguments().getStringArray("extraargs");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle icic){
		
		View v=inflater.inflate(R.layout.flickrimagegrid,container,false);
		thegrid = (GridView)v.findViewById(R.id.thegrid);
		
		thegrid.getViewTreeObserver().addOnGlobalLayoutListener(
				new ViewTreeObserver.OnGlobalLayoutListener() {
					
					@Override
					public void onGlobalLayout() {
						if(thegrid.getWidth()!=0){
							Log.v("gridwidth",thegrid.getWidth()+"");
							thegrid.getViewTreeObserver().removeGlobalOnLayoutListener(this);
							theimageAdapter = new LazyImageAdapter(getActivity(), urls,thegrid.getWidth());
							thegrid.setAdapter(theimageAdapter);
							thegrid.setOnItemClickListener(FlickrFragment.this);
						}
					}
				}
				);
		
		

		handleKeys = new HandleOauthKeys(getActivity());
		service = new ServiceBuilder().provider(FlickrNewApi.class)
				.callback("vivek://com.life.flicasa")
				.apiKey(Config.FLAPIKEY.getValue())
				.apiSecret(Config.FLAPISECRET.getValue()).debug().build();

		Token myaccessToken = handleKeys.getAccessToken();
		String theaccessToken = myaccessToken.getToken();
		
		

		
	
		FlickrRestTest theRestFlickr = new FlickrRestTest(getActivity(),
				service, myaccessToken,getCallback(),myhandler,getExtraargs());
		Thread myThread = new Thread(theRestFlickr);
		myThread.start();
		
		showBusyDialog("Flicasa is Loading please Wait");
		Log.v("at", theaccessToken + " -sec " + myaccessToken.getSecret());
		return v;
		
	}
	
		
	

	public void showBusyDialog(String message) {
		busyDialog = new Dialog(getActivity(), R.style.lightbox_dialog);
		busyDialog.setContentView(R.layout.lightbox_dialog);
		((TextView) busyDialog.findViewById(R.id.dialogText)).setText(message);

		busyDialog.show();
	}

	public void dismissBusyDialog() {
		if (busyDialog != null)
			busyDialog.dismiss();

		busyDialog = null;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		try{
			theflickrShowPhotos=(FlickrShowPhotos)activity;
		}catch(ClassCastException e){
			 throw new ClassCastException(activity.toString() + " must implement flickrShowPhotos Listener");
		}
	}

	@Override
	public void onItemClick(AdapterView<?> gv, View arg1, int pos, long arg3) {
		LazyImageAdapter la=(LazyImageAdapter)gv.getAdapter();
		ArrayList<HashMap<String,String>> local=la.theurlArraywithtitles;
		HashMap<String,String> hm=local.get(pos);
		String thecallback=getCallback();
		String theId=null;
		Log.v("thecallbackiu",thecallback);
		if(thecallback=="getPhotoSetsOfUser"){
			Log.v("thecallbackiu",thecallback);
			theId=hm.get("photosetid");
			theflickrShowPhotos.showPhotos(theId);
			
		}
		if(thecallback=="getGalleries"){
			Log.v("thecallbackiu",thecallback);
			theId=hm.get("galleryid");
			theflickrShowPhotos.showPhotosFromGallery(theId);
		}
		
	
		
		
	}
	public interface FlickrShowPhotos{
		public void showPhotos(String i);
		public void showPhotosFromGallery(String i);
	}

	

}