package com.vivek.flickr;

import java.util.ArrayList;
import java.util.HashMap;

public class MessageObj{
	private ArrayList<HashMap<String,String>> data;
	
	public MessageObj(ArrayList<HashMap<String,String>> data){
		this.data=data;
	}
	public ArrayList<HashMap<String,String>> getArrayList(){
		return data;
	}
}