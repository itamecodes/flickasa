package com.vivek.flickr;

import java.util.HashMap;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PicasaActivity extends Fragment {
	WebView wv;
	TextView tv;
	HandleOauthKeysGoogle thekeyHandler;
	final Handler myhandler=new Handler(){
		@Override
		public void handleMessage(Message msg){
			int what=msg.what;
			
			switch(what){
			case(1):{
				String thestr=(String)msg.obj;
				wv.loadUrl(thestr);
				break;
			}
			case(2):{
				MessageObjGoogle theobj=(MessageObjGoogle) msg.obj;
				String at=theobj.getAt();
				String rt=theobj.getRt();
				String it=theobj.getIt();
				thekeyHandler.setAccessToken(at, rt, it);
				((LinearLayout)wv.getParent()).removeView(wv);
				break;
			}
			
			}
			
		}
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle icic){
	View v=inflater.inflate(R.layout.picasa, container,false);
	tv=(TextView) getActivity().findViewById(R.id.ptv);
	wv=(WebView) getActivity().findViewById(R.id.pwebview);
	thekeyHandler=new HandleOauthKeysGoogle(getActivity());
	HashMap<String,String> thekeys=(HashMap<String,String>)thekeyHandler.getAccessToken();
	String myat=thekeys.get("atoken");
	String myrt=thekeys.get("rtoken");
	
	if(myat.equals("notset")){
		GoogleOAuth picasaAuth=new GoogleOAuth(getActivity(),myhandler,tv);
		Thread thethread=new Thread(picasaAuth);
		thethread.start();
		}else{
			//this is a test case.. normally we will refresh the access token whenever we get 401 from server
			GoogleOAuth picasaAuth=new GoogleOAuth(getActivity(),myhandler,tv,myrt);
			Thread thethread=new Thread(picasaAuth);
			thethread.start();
			
			
		}
	
	wv.setWebViewClient(new WebViewClient(){
		@Override
	    public boolean shouldOverrideUrlLoading(WebView view, String url) {
	       return super.shouldOverrideUrlLoading(view, url);
	    }
	});
	return v;
	}
	
	
}
