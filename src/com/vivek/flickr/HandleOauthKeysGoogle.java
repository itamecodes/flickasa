package com.vivek.flickr;

import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class HandleOauthKeysGoogle {
	Activity c;
	Context context;
	
	final String ACCESS_TOKEN="accesstoken";
	final String IDTOKEN="idtoken";
	final String REFRESHTOKEN="refreshtoken";
    
	HashMap<String,String> map=new HashMap<String,String>();
	
	SharedPreferences thestorage;
	
	public HandleOauthKeysGoogle(Activity act){
		this.c=act;
		this.context=act.getApplicationContext();
		thestorage=context.getSharedPreferences("PicassaStorage", Context.MODE_PRIVATE);
	}
	
	public HashMap<String,String> getAccessToken(){
		String theatoken=thestorage.getString(ACCESS_TOKEN,"notset");
		String thertoken=thestorage.getString(REFRESHTOKEN,"myrtui");
		String theitoken=thestorage.getString(IDTOKEN,"myit");
		map.put("atoken",theatoken);
		map.put("rtoken",thertoken);
		map.put("itoken",theitoken);
		return map;
		
	}
	public void setAccessToken(String at,String rt,String it){
		SharedPreferences.Editor theeditor=thestorage.edit();
		theeditor.putString(ACCESS_TOKEN, at);
		theeditor.putString(REFRESHTOKEN, rt);
		theeditor.putString(IDTOKEN, it);
		theeditor.commit();
	}
	

}
