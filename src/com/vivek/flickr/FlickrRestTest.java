package com.vivek.flickr;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class FlickrRestTest implements Runnable {
	String apiKey = "496a8ac6ca46325e6cefcb924d539296";
	String apiSecret = "685682a92c03ebdc";
	private static final String PROTECTED_RESOURCE_URL = "http://api.flickr.com/services/rest/";
	Activity c;
	OAuthService service;
	Token accessToken;
	OAuthRequest request;
	String themethod;
	String[] args;
	JSONObject jo;
	Handler myhandler;
	ArrayList<HashMap<String, String>> imageArray = new ArrayList<HashMap<String, String>>();
	Class params[];
	Object values[];
	Config config;

	public FlickrRestTest(Activity act, OAuthService theservice, Token aT,
			String callback, Handler thehandler, String... args) {
		c = act;
		accessToken = aT;
		service = theservice;
		themethod = callback;
		myhandler = thehandler;
		request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
		int count = args.length;
		if (count == 0) {
			params = new Class[] {};
			values = new Object[] {};
		} else {
			params = new Class[count];
			values = new Object[count];
			for (int i = 0; i < count; i++) {
				params[i] = String.class;
				values[i] = new String(args[i]);
				// very important from here
			}
		}
		// imageArray=urls;
	}

	@Override
	public void run() {
		try {
			Method method = this.getClass().getMethod(themethod, params);
			method.invoke(this, values);
		} catch (SecurityException e) {
			Log.v("viver", "sc" + e.getLocalizedMessage());
		} catch (NoSuchMethodException e) {
			Log.v("viver1", "sc" + e.getLocalizedMessage());
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

	}

	public void getActivityUserPhotos() {
		String method = "flickr.activity.userPhotos";
		request.addQuerystringParameter("method", method);
		request.addQuerystringParameter("format", "json");

		service.signRequest(accessToken, request);
		Response response = request.send();
		Log.v("vivek", response.getBody());
	}

	public void getActivityPhotosCommentedOn() {
		String method = "flickr.activity.userComments";
		request.addQuerystringParameter("method", method);
		request.addQuerystringParameter("format", "json");

		service.signRequest(accessToken, request);
		Response response = request.send();
		Log.v("vivek", response.getBody());
	}



	public void getPhotoSetsOfUser(String per_page, String page, String userid) {
		String method = "flickr.photosets.getList";
		request.addQuerystringParameter("method", method);
		request.addQuerystringParameter("api_key", Config.FLAPIKEY.getValue());
		request.addQuerystringParameter("per_page", per_page);
		request.addQuerystringParameter("page", page);
		request.addQuerystringParameter("user_id", userid);
		request.addQuerystringParameter("format", "json");
		service.signRequest(accessToken, request);
		Response response = request.send();
		String locresp = stripJsonFlickr(response);
		try {
			jo = new JSONObject(locresp);
			JSONObject photosets = jo.getJSONObject("photosets");
			String pageno = photosets.getString("page");
			String totalpages = photosets.getString("pages");
			String totalphotoset = photosets.getString("total");
			JSONArray photosetarray = photosets.getJSONArray("photoset");
			for (int i = 0; i < photosetarray.length(); i++) {
				JSONObject locJo = photosetarray.getJSONObject(i);
				String photosetID = locJo.getString("id");
				String imgid = locJo.getString("primary");
				String secret = locJo.getString("secret");
				String server = locJo.getString("server");
				String farm = locJo.getString("farm");
				String photosinPhotoset = locJo.getString("photos");

				JSONObject titleset = locJo.getJSONObject("title");
				String titleimage = titleset.getString("_content");

				String theurl = "http://farm" + farm + ".staticflickr.com/"
						+ server + "/" + imgid + "_" + secret + ".jpg";
				HashMap<String, String> thehm = new HashMap<String, String>();
				thehm.put("title", titleimage);
				thehm.put("theurl", theurl);
				thehm.put("photosetid", photosetID);

				imageArray.add(thehm);
			}

		} catch (JSONException e) {
			Log.v("vivekphotosetresp", "jsonexception");
		}
		Log.v("vivekphotosetrep", locresp);
		Log.v("vivekphotoset", "user_id=" + userid);

		MessageObj themsgObj = new MessageObj(imageArray);
		Message themessage = myhandler.obtainMessage(2, themsgObj);
		myhandler.sendMessage(themessage);

	}

	private String stripJsonFlickr(Response response) {
		String locResp = response.getBody().replace("jsonFlickrApi(", "");
		locResp = locResp.substring(0, locResp.length() - 1);
		return locResp;
	}

	public void showImagesFromPhotoset(String photosetid, String per_page,
			String page, String userid) {
		String method = "flickr.photosets.getPhotos";
		request.addQuerystringParameter("method", method);
		request.addQuerystringParameter("api_key", Config.FLAPIKEY.getValue());
		request.addQuerystringParameter("per_page", per_page);
		request.addQuerystringParameter("page", page);
		request.addQuerystringParameter("user_id", userid);
		request.addQuerystringParameter("photoset_id", photosetid);
		request.addQuerystringParameter("format", "json");
		service.signRequest(accessToken, request);
		Response response = request.send();
		String locresp = stripJsonFlickr(response);

		/*
		 * response format { "photoset": { "id": "72157630325618266", "primary":
		 * "6239960126", "owner": "68584477@N05", "ownername": "aliceni19",
		 * "photo": [ { "id": "6239960126", "secret": "214c95edf7", "server":
		 * "6163", "farm": 7, "title": "DSC01371", "isprimary": 1 }, { "id":
		 * "6239442175", "secret": "b761fe1fe3", "server": "6155", "farm": 7,
		 * "title": "DSC01370", "isprimary": 0 } ], "page": 1, "per_page":
		 * "500", "perpage": "500", "pages": 1, "total": 2 }, "stat": "ok" }
		 */
		try {
			jo = new JSONObject(locresp);
			JSONObject outlocjo = jo.getJSONObject("photoset");

			String totalPhotosinPhotoset = outlocjo.getString("total");
			JSONArray photoarray = outlocjo.getJSONArray("photo");
			for (int i = 0; i < photoarray.length(); i++) {
				JSONObject locJo = photoarray.getJSONObject(i);
				String imgid = locJo.getString("id");
				String secret = locJo.getString("secret");
				String server = locJo.getString("server");
				String farm = locJo.getString("farm");
				String titleimage = locJo.getString("title");

				String theurl = "http://farm" + farm + ".staticflickr.com/"
						+ server + "/" + imgid + "_" + secret + ".jpg";
				HashMap<String, String> thehm = new HashMap<String, String>();
				thehm.put("title", titleimage);
				thehm.put("theurl", theurl);

				imageArray.add(thehm);
			}

		} catch (JSONException e) {
			Log.v("picsinphotoset", "error parsing json");
		}
		MessageObj themsgObj = new MessageObj(imageArray);
		Message themessage = myhandler.obtainMessage(2, themsgObj);
		myhandler.sendMessage(themessage);

	}

	public void getPhotoNotInSetOfUser() {
		String method = "flickr.photos.getNotInSet";
		request.addQuerystringParameter("method", method);
		request.addQuerystringParameter("format", "json");
		request.addQuerystringParameter("api_key", apiKey);
		request.addQuerystringParameter("per_page", "20");
		service.signRequest(accessToken, request);
		Response response = request.send();
		String locResp = response.getBody().replace("jsonFlickrApi(", "");
		locResp = locResp.substring(0, locResp.length() - 1);
		Log.v("locresp", locResp);
		try {
			jo = new JSONObject(locResp);
			JSONObject jPhotos = jo.getJSONObject("photos");
			Log.v("vivek", "1");
			String pageno = jPhotos.getString("page");
			Log.v("vivek", "2" + pageno);
			String totpages = jPhotos.getString("pages");
			Log.v("vivek", "3" + totpages);
			String totPhots = jPhotos.getString("total");
			Log.v("vivek", "4" + totPhots);
			JSONArray ja = jPhotos.getJSONArray("photo");
			Log.v("vivek", "5");
			String url = "http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg";
			for (int i = 0; i < (ja.length() - 1); i++) {
				JSONObject locJo = ja.getJSONObject(i);
				String imgid = locJo.getString("id");
				String secret = locJo.getString("secret");
				String server = locJo.getString("server");
				String farm = locJo.getString("farm");
				String theurl = "http://farm" + farm + ".staticflickr.com/"
						+ server + "/" + imgid + "_" + secret + ".jpg";
				String titleimage = locJo.getString("title");
				HashMap<String, String> thehm = new HashMap<String, String>();
				thehm.put("title", titleimage);
				thehm.put("theurl", theurl);

				imageArray.add(thehm);

			}
			/*
			 * 
			 * jsonFlickrApi( {"photos":{"page":1, "pages":4, "perpage":20,
			 * "total":"77", "photo":[ {"id":"6239439357",
			 * "owner":"68584477@N05", "secret":"0b722b106c", "server":"6233",
			 * "farm":7, "title":"DSC01368", "ispublic":0, "isfriend":0,
			 * "isfamily":1}, {"id":"6239440863", "owner":"68584477@N05",
			 * "secret":"c2e6c7c713", "server":"6169", "farm":7,
			 * "title":"DSC01369", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239442175", "owner":"68584477@N05",
			 * "secret":"b761fe1fe3", "server":"6155", "farm":7,
			 * "title":"DSC01370", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239960126", "owner":"68584477@N05",
			 * "secret":"214c95edf7", "server":"6163", "farm":7,
			 * "title":"DSC01371", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239961272", "owner":"68584477@N05",
			 * "secret":"bf410a3c3f", "server":"6055", "farm":7,
			 * "title":"DSC01372", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239445855", "owner":"68584477@N05",
			 * "secret":"899ef92253", "server":"6222", "farm":7,
			 * "title":"DSC01373", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239447463", "owner":"68584477@N05",
			 * "secret":"8a71b5a787", "server":"6217", "farm":7,
			 * "title":"DSC01378", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239965386", "owner":"68584477@N05",
			 * "secret":"7444763105", "server":"6212", "farm":7,
			 * "title":"DSC01379", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239966400", "owner":"68584477@N05",
			 * "secret":"53148d860b", "server":"6162", "farm":7,
			 * "title":"DSC01381", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239967414", "owner":"68584477@N05",
			 * "secret":"173e2fbf5b", "server":"6119", "farm":7,
			 * "title":"DSC01382", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239451751", "owner":"68584477@N05",
			 * "secret":"44d328323f", "server":"6226", "farm":7,
			 * "title":"DSC01380", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239452835", "owner":"68584477@N05",
			 * "secret":"a4aa2203b8", "server":"6234", "farm":7,
			 * "title":"DSC01383", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239453751", "owner":"68584477@N05",
			 * "secret":"9258751d08", "server":"6109", "farm":7,
			 * "title":"DSC01384", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239971380", "owner":"68584477@N05",
			 * "secret":"b5a2c8de18", "server":"6221", "farm":7,
			 * "title":"DSC01386", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239972148", "owner":"68584477@N05",
			 * "secret":"feabf523ba", "server":"6093", "farm":7,
			 * "title":"DSC01387", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239456577", "owner":"68584477@N05",
			 * "secret":"1c4b659b10", "server":"6239", "farm":7,
			 * "title":"DSC01388", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239974194", "owner":"68584477@N05",
			 * "secret":"34c0161621", "server":"6233", "farm":7,
			 * "title":"DSC01389", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239975290", "owner":"68584477@N05",
			 * "secret":"72a76ef931", "server":"6039", "farm":7,
			 * "title":"DSC01390", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239459769", "owner":"68584477@N05",
			 * "secret":"56d82a0df6", "server":"6160", "farm":7,
			 * "title":"DSC01392", "ispublic":0, "isfriend":0, "isfamily":1},
			 * {"id":"6239460755", "owner":"68584477@N05",
			 * "secret":"077cdc4cf9", "server":"6096", "farm":7,
			 * "title":"DSC01391", "ispublic":0, "isfriend":0, "isfamily":1}]},
			 * "stat":"ok"})
			 */

		} catch (JSONException e) {
			Log.v("vivek",
					"unable to create the main json object"
							+ e.getLocalizedMessage());
		}
		Log.v("vivek123", "hi" + imageArray.toString());
		MessageObj themsgObj = new MessageObj(imageArray);
		Message themessage = myhandler.obtainMessage(2, themsgObj);
		myhandler.sendMessage(themessage);

		// myhandler.sendEmptyMessage(2);
		Log.v("vivek", response.getBody());
	}

	public void getGalleries(String userid, String pageno, String per_page) {
		/*
		 * { "galleries": { "total": 2, "page": 1, "pages": 1, "per_page": 5,
		 * "user_id": "68584477@N05", "gallery": [ { "id":
		 * "68579137-72157630384785444", "url":
		 * "http:\/\/www.flickr.com\/photos\/asklife\/galleries\/72157630384785444",
		 * "owner": "68584477@N05", "username": "aliceni19", "iconserver": 0,
		 * "iconfarm": 0, "primary_photo_id": "4821390579", "date_create":
		 * "1341224649", "date_update": "1341224672", "count_photos": 1,
		 * "count_videos": 0, "count_views": 0, "count_comments": 0, "title": {
		 * "_content": "MyGallerytwo" }, "description": { "_content":
		 * "test galleries" }, "primary_photo_server": "4140",
		 * "primary_photo_farm": 5, "primary_photo_secret": "31d4ee7864" }, {
		 * "id": "68579137-72157630384769506", "url":
		 * "http:\/\/www.flickr.com\/photos\/asklife\/galleries\/72157630384769506",
		 * "owner": "68584477@N05", "username": "aliceni19", "iconserver": 0,
		 * "iconfarm": 0, "primary_photo_id": "62716111", "date_create":
		 * "1341224549", "date_update": "1341224619", "count_photos": 1,
		 * "count_videos": 0, "count_views": 0, "count_comments": 0, "title": {
		 * "_content": "MyGalleryone" }, "description": { "_content":
		 * "All artstic images" }, "primary_photo_server": 33,
		 * "primary_photo_farm": 1, "primary_photo_secret": "55947072e6" } ] },
		 * "stat": "ok" }
		 */
		String method = "flickr.galleries.getList";
		request.addQuerystringParameter("method", method);
		request.addQuerystringParameter("api_key", Config.FLAPIKEY.getValue());
		request.addQuerystringParameter("per_page", per_page);
		request.addQuerystringParameter("page", pageno);
		request.addQuerystringParameter("user_id", userid);
		request.addQuerystringParameter("format", "json");
		service.signRequest(accessToken, request);
		Response response = request.send();
		String locresp = stripJsonFlickr(response);
		try {
			jo = new JSONObject(locresp);
			JSONObject galleries = jo.getJSONObject("galleries");
			String page = galleries.getString("page");
			String totalpages = galleries.getString("pages");
			String totalphotoset = galleries.getString("total");
			JSONArray galleryarray = galleries.getJSONArray("gallery");
			for (int i = 0; i < galleryarray.length(); i++) {
				JSONObject locJo = galleryarray.getJSONObject(i);
				String galleryID = locJo.getString("id");
				String imgid = locJo.getString("primary_photo_id");
				String secret = locJo.getString("primary_photo_secret");
				String server = locJo.getString("primary_photo_server");
				String farm = locJo.getString("primary_photo_farm");

				JSONObject titleset = locJo.getJSONObject("title");
				String titleimage = titleset.getString("_content");

				String theurl = "http://farm" + farm + ".staticflickr.com/"
						+ server + "/" + imgid + "_" + secret + ".jpg";
				HashMap<String, String> thehm = new HashMap<String, String>();
				thehm.put("title", titleimage);
				thehm.put("theurl", theurl);
				thehm.put("galleryid", galleryID);

				imageArray.add(thehm);
			}

		} catch (JSONException e) {
			Log.v("vivekphotosetresp", "jsonexception");
		}
		Log.v("vivekphotosetrep", locresp);
		Log.v("vivekphotoset", "user_id=" + userid);

		MessageObj themsgObj = new MessageObj(imageArray);
		Message themessage = myhandler.obtainMessage(2, themsgObj);
		myhandler.sendMessage(themessage);
	}

	public void getPhotosFromGallery(String galleryid,String per_page,String page,String userid) {
		/*
		 * { "photos": { "page": 1, "pages": 1, "perpage": "500", "total": 1,
		 * 
		 * "photo": [ { "id": "4821390579", "owner": "25302351@N03", "secret":
		 * "31d4ee7864", "server": "4140", "farm": 5, "title":
		 * "Its all a matter of scale ...... ali-cat passes crown princess at greenock whilst two boys take in the scene"
		 * , "ispublic": 1, "isfriend": 0, "isfamily": 0, "is_primary": 1,
		 * "has_comment": 0 } ] }, "stat": "ok" }
		 */
		String method = "flickr.galleries.getPhotos";
		request.addQuerystringParameter("method", method);
		request.addQuerystringParameter("api_key", Config.FLAPIKEY.getValue());
		request.addQuerystringParameter("per_page", per_page);
		request.addQuerystringParameter("page", page);
		request.addQuerystringParameter("user_id", userid);
		request.addQuerystringParameter("gallery_id", galleryid);
		request.addQuerystringParameter("format", "json");
		service.signRequest(accessToken, request);
		Response response = request.send();
		String locresp = stripJsonFlickr(response);
        Log.v("galleryphotos",locresp);
		try {
			jo = new JSONObject(locresp);
			Log.v("galleryphotos","1");
			JSONObject outlocjo = jo.getJSONObject("photos");
			Log.v("galleryphotos","2");
			String totalPhotosinPhotoset = outlocjo.getString("total");
			Log.v("galleryphotos","3");
			JSONArray photoarray = outlocjo.getJSONArray("photo");
			Log.v("galleryphotos","4");
			for (int i = 0; i <= photoarray.length(); i++) {
				JSONObject locJo = photoarray.getJSONObject(i);
				Log.v("galleryphotos","5");
				String imgid = locJo.getString("id");
				Log.v("galleryphotos","6");
				String secret = locJo.getString("secret");
				Log.v("galleryphotos","7");
				String server = locJo.getString("server");
				Log.v("galleryphotos","8");
				String farm = locJo.getString("farm");
				Log.v("galleryphotos","9");
				String titleimage = locJo.getString("title");
				Log.v("galleryphotos","10");

				String theurl = "http://farm" + farm + ".staticflickr.com/"
						+ server + "/" + imgid + "_" + secret + ".jpg";
				Log.v("galleryphotos","11");
				HashMap<String, String> thehm = new HashMap<String, String>();
				Log.v("galleryphotos","12");
				thehm.put("title", titleimage);
				Log.v("galleryphotos","13");
				thehm.put("theurl", theurl);
				Log.v("galleryphotos","14");

				imageArray.add(thehm);
			}

		} catch (JSONException e) {
			Log.v("picsinphotoset", "error parsing json"+e.getLocalizedMessage());
		}
		MessageObj themsgObj = new MessageObj(imageArray);
		Message themessage = myhandler.obtainMessage(2, themsgObj);
		myhandler.sendMessage(themessage);
		
	}
}
