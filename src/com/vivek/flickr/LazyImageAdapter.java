package com.vivek.flickr;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class LazyImageAdapter extends BaseAdapter {
	
	ArrayList<HashMap<String,String>> theurlArraywithtitles;
	Activity c;
	AdapterImageLoader imageLoader;
	int cellwidth;
	public LazyImageAdapter(Activity act,ArrayList<HashMap<String,String>> urls,int reqwidth){
		c=act;
		theurlArraywithtitles=urls;
		cellwidth=reqwidth/2;
		imageLoader=new AdapterImageLoader(c.getApplicationContext());
	}

	@Override
	public int getCount() {
		return theurlArraywithtitles.size();
	}

	@Override
	public Object getItem(int pos) {
		return theurlArraywithtitles.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}
	static class TheRow{
		ImageView im;
		TextView tv;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup vg) {
			View theview=convertView;
			if(theview==null){
				LayoutInflater theinf=c.getLayoutInflater();
				theview=theinf.inflate(R.layout.gridelement, null);
				TheRow myrow=new TheRow();
				myrow.im=(ImageView)theview.findViewById(R.id.theimage);
				myrow.tv=(TextView)theview.findViewById(R.id.thetitle);
				myrow.tv.setBackgroundColor(Color.argb(125, 000,000, 000));
				theview.setTag(myrow);
			}
			TheRow myrow=(TheRow)theview.getTag();
			HashMap<String,String> thelochm=theurlArraywithtitles.get(pos);
			String url=thelochm.get("theurl");
			String text=thelochm.get("title");
			myrow.tv.setText(text);
			Log.v("vivurl",url);
			imageLoader.DisplayImage(url, myrow.im,cellwidth,275);
			
		return theview;
	}

}
